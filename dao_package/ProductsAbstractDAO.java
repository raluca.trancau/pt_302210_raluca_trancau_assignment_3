
package dao_package;

import model.Products;
/**
 * – extinde clasa AbstractDAO; aceasta clasa nu are variabile instanta si contine doar un constructor;
 */
public class ProductsAbstractDAO extends AbstractDAO<Products> {
	public ProductsAbstractDAO() {
		super();
	}
}
