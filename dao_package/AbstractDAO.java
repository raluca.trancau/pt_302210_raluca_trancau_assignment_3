
package dao_package;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.MyConnection;
/**
 * – contine un obiect de o Clasa generica si o constanta de tipul Logger; 
 * - clasa contine un constructor, metode care creeaza query-urile pentru operatiile de select, select all, insert, delete si update, 
 * totodata continand si metode care realizeaza conexiunea la BD si executa query-urile;
 */
public class AbstractDAO<T> {
    
        private final Class<T> type;
	protected static final Logger log = Logger.getLogger(AbstractDAO.class.getName());
	

	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public String createSelectString(String field) {
		StringBuilder builtString = new StringBuilder();
		 builtString.append("SELECT * FROM ");
		 builtString.append(type.getSimpleName());
		 builtString.append(" WHERE " + field + " =?");
		return  builtString.toString();
	}

	public String createSelectAllString() {
		StringBuilder  builtString = new StringBuilder();
		 builtString.append("SELECT * FROM ");
		 builtString.append(type.getSimpleName());
		return  builtString.toString();
	}

	public String createInsertString() {
		StringBuilder  builtString = new StringBuilder();
		 builtString.append("INSERT INTO ");
		 builtString.append(type.getSimpleName());
		 builtString.append(" VALUES (");
		for (int i = 0; i < type.getDeclaredFields().length - 1; i++) {
			 builtString.append(" ? , ");
		}
		 builtString.append(" ? )");
		return  builtString.toString();
	}
        public String createDeleteString(String field) {
		StringBuilder  builtString = new StringBuilder();
		 builtString.append("DELETE FROM ");
		 builtString.append(type.getSimpleName());
		 builtString.append(" WHERE " + field + " =?");
		return  builtString.toString();
	}
	public String createUpdateString(String field1, String field2) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("UPDATE ");
		stringBuilder.append(type.getSimpleName());
		stringBuilder.append(" SET ");
		stringBuilder.append(field1 + " =? ");
		stringBuilder.append(" WHERE " + field2 + " =?");
		return stringBuilder.toString();
	}

	

	public T findByID(int id) {
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectString(type.getDeclaredFields()[0].getName());
		try {
			conn=MyConnection.getConnection(conn);
			statement = conn.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.first() == false) {
				return null;
			}
			return makeObjects(resultSet).get(0);
		} catch (SQLException exp) {
			log.log(Level.WARNING, type.getName() + "FROM DAO: findById" + exp.getMessage());
		} finally {
			MyConnection.closeResultSet(resultSet);
			MyConnection.closePreparedStatement(statement);
			MyConnection.closeConnection(conn);
		}
		return null;
	}

	private ArrayList<T> makeObjects(ResultSet resultSet) {
		ArrayList<T> l = new ArrayList<T>(1);
		try {
			if (resultSet.first()) {
				do {
					T inst = type.newInstance();
					for (Field field : type.getDeclaredFields()) {
						Object value = resultSet.getObject(field.getName());
						PropertyDescriptor propDescriptor = new PropertyDescriptor(field.getName(), type);
						Method method = propDescriptor.getWriteMethod();
						method.invoke(inst, value);
					}
					l.add(inst);
				} while (resultSet.next());
			}
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| SQLException | IntrospectionException exp) {
			log.log(Level.WARNING, type.getName() + "FROM DAO: makeObjects: " + exp.getMessage());
		}
		return l;
	}

	public void addObject(T object) {
		Connection conn = null;
		PreparedStatement statement = null;
		String query = createInsertString();
		try {
			conn = MyConnection.getConnection(conn);
			statement = conn.prepareStatement(query);
			int i = 1;
			for (Field field : type.getDeclaredFields()) {
				PropertyDescriptor propDescriptor = new PropertyDescriptor(field.getName(), type);
				Method method = propDescriptor.getReadMethod();
				Object value = method.invoke(object);
				statement.setObject(i, value);
				i++;
			}
			statement.executeUpdate();
		} catch (IllegalArgumentException | IllegalAccessException | SQLException | InvocationTargetException
				| IntrospectionException exp) {
			log.log(Level.WARNING, type.getName() + "FROM DAO: addObject: " + exp.getMessage());
		} finally {
			MyConnection.closePreparedStatement(statement);
			MyConnection.closeConnection(conn);
		}
	}
        	public void deleteObjectByID(int id) {
		Connection conn = null;
		PreparedStatement statement = null;
		String query = createDeleteString(type.getDeclaredFields()[0].getName());
		try {
			conn = MyConnection.getConnection(conn);
			statement = conn.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException exp) {
			log.log(Level.WARNING, type.getName() + "FROM DAO: deleteById: " + exp.getMessage());
		} finally {
			MyConnection.closePreparedStatement(statement);
			MyConnection.closeConnection(conn);
		}
	}

	public void updateField(String fieldNewValue, String fieldCond, Object obj1, Object obj2) {
		Connection conn = null;
		PreparedStatement statement = null;
		String query = createUpdateString(fieldNewValue, fieldCond);
		try {
			conn = MyConnection.getConnection(conn);
			statement = conn.prepareStatement(query);
			statement.setObject(1, obj1);
			statement.setObject(2, obj2);
			statement.executeUpdate();
		} catch (IllegalArgumentException | SQLException exp) {
			log.log(Level.WARNING, type.getName() + "FROM DAO: updateField: " + exp.getMessage());
		} finally {
			MyConnection.closePreparedStatement(statement);
			MyConnection.closeConnection(conn);
		}
	}



	public List<T> findAll() {
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectAllString();
		try {
			conn = MyConnection.getConnection(conn);
			statement = conn.prepareStatement(query);
			resultSet = statement.executeQuery();
			return makeObjects(resultSet);
		} catch (SQLException exp) {
			log.log(Level.WARNING, type.getName() + "FROM DAO: findAll: " + exp.getMessage());
		} finally {
			MyConnection.closeResultSet(resultSet);
			MyConnection.closePreparedStatement(statement);
			MyConnection.closeConnection(conn);
		}
		return Collections.emptyList();
	}
}
