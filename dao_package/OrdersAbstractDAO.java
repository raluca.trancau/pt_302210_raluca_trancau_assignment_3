
package dao_package;

import model.Orders;
/**
 * – extinde clasa AbstractDAO; aceasta clasa nu are variabile instanta si contine doar un constructor;
 */
public class OrdersAbstractDAO extends AbstractDAO<Orders> {
	public OrdersAbstractDAO() {
		super();
	}
}
