
package business_layer;

import java.lang.reflect.Field;
import java.util.ArrayList;

import dao_package.ClientsAbstractDAO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Clients;
/**
 * – contine doua variabile instanta: un obiect de tipul ClientsAbstractDAO, 
 * care are rolul de a efectua legatura intre Business Logic Layer si Data Access Layer, 
 * si un obiect de tipul ClientsValidation, care are rolul de a valida datele referitoare la un client 
 * inainte de efectuarea oricarei operatii cu datele  corespunzatoare clientului; 
 * - aceasta clasa contine constructor si cate o metoda pentru fiecare operatie de baza asupra tabelului Clients (adaugarea unui client, 
 * stergerea unui client, editarea numelui clientului si vizualizarea tuturor clientilor din tabela Clients);
 */
public class ClientsBLL {
    
	private ClientsAbstractDAO clDAO;
	private ClientsValidation clValidation;

	public ClientsBLL(ClientsAbstractDAO clDAO, ClientsValidation clValidation) {
		super();
		this.clDAO = clDAO;
		this.clValidation = clValidation;
	}

	public void addClient(Clients client) {
		if (clValidation.isValid()) {
			if (clDAO.findByID(client.getID()) == null) {
				clDAO.addObject(client);
			} else {
				System.out.println("This ID already exists!");
			}
		}
	}
        public void deleteClient(int ID) {
		if (clDAO.findByID(ID) != null) {
			clDAO.deleteObjectByID(ID);
		} else {
			System.out.println("Searched client does not exist!");
		}
	}
	public void editClName(int ID, String name) {
		if (name.length() <= 30) {
			if (clDAO.findByID(ID) != null) {
				clDAO.updateField("name", "ID", name, ID);
			} else {
				System.out.println("Searched client does not exist!");
			}
		} else {
			System.out.println("The new name is too long!");
		}
	}

	

	public Object[][] showAll(){
                
		List<Clients> clients = new ArrayList<Clients>();
		clients = clDAO.findAll();
		int nrLin = 0;
		for (Clients client : clients) {
			nrLin++;
		}
		Object[][] table= new Object[nrLin][Clients.class.getDeclaredFields().length];
		int i = 0;
		for (Clients client : clients) {
			for (int j = 0; j < Clients.class.getDeclaredFields().length; j++) {
				try {
					Field f = Clients.class.getDeclaredFields()[j];
					f.setAccessible(true);
					table[i][j] = f.get(client);
				}catch (SecurityException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException ex) {
                                Logger.getLogger(ClientsBLL.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IllegalAccessException ex) {
                                Logger.getLogger(ClientsBLL.class.getName()).log(Level.SEVERE, null, ex);
                            }
			}
			i++;
		}
		return table;
	}
}
