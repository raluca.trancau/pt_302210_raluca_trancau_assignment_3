
package business_layer;

import java.lang.reflect.Field;
import java.util.ArrayList;


import dao_package.ProductsAbstractDAO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Clients;
import model.Products;
/**
 * – contine doua variabile instanta: un obiect de tipul ProductsAbstractDAO, 
 * care are rolul de a efectua legatura intre Business Logic Layer si Data Access Layer, 
 * si un obiect de tipul ProductsValidation, care are rolul de a valida datele referitoare la un produs 
 * inainte de efectuarea oricarei operatii asupra lui; 
 * - pe langa constructor, clasa contine si cate o metoda pentru fiecare operatie de baza asupra tabelului Products 
 * (adaugarea unui produs, stergerea unui produs, editarea numelui produsului, editarea stocului produsului, 
 * editarea cantitatii produsului si vizualizarea tuturor produselor din tabela Products);
 */
public class ProductsBLL {
    
	private ProductsAbstractDAO prodDAO;
	private ProductsValidation prodValidation;

	public ProductsBLL(ProductsAbstractDAO prodDAO, ProductsValidation prodValidation) {
		super();
		this.prodDAO = prodDAO;
		this.prodValidation = prodValidation;
	}
        


	public void addProduct(Products product) {
		if (prodValidation.isValid()) {
			if (prodDAO.findByID(product.getProdID()) == null) {
				prodDAO.addObject(product);
			} else if (prodDAO.findByID(product.getProdID()).getProdName().equals(product.getProdName())) {
				int newProdStock = prodDAO.findByID(product.getProdID()).getStock() + product.getStock();
				this.editProdStock(product.getProdID(), newProdStock);
			} else {
				System.out.println("The product's ID already exists!");
			}
		}
	}
        public void deleteProduct(int ProdID) {
		if (prodDAO.findByID(ProdID) != null) {
			prodDAO.deleteObjectByID(ProdID);
		} else {
			System.out.println("The product does not exist!");
		}
	}
	public int editProdName(int ProdID, String newProdName) {
		if (newProdName.length() <= 30) {
			if (prodDAO.findByID(ProdID) != null) {
				prodDAO.updateField("prodName", "prodID", newProdName, ProdID);
                                return 1;
			} else {
				System.out.println("The product does not exist!");
                                return 0;
			}
		} else {
			System.out.println("The new name is too long!");
                        return 0;
		}
	}
        public int editProdStock(int ProdID, int newProdStock) {
		if (newProdStock > 0) {
			if (prodDAO.findByID(ProdID) != null) {
				prodDAO.updateField("stock", "prodID", newProdStock, ProdID);
                                return 1;
			} else {
				System.out.println("The product does not exist!");
                                return 0;
			}
		} else {
			System.out.println("The stock must be a positive number!");
                        return 0;
		}
	}

	public int editProdPrice(int ProdID, float newProdPrice) {
		if (newProdPrice > 0) {
			if (prodDAO.findByID(ProdID) != null) {
				prodDAO.updateField("price", "prodID", newProdPrice, ProdID);
                                return 1;
			} else {
				System.out.println("The product does not exist!");
                                return 0;
			}
		} else {
			System.out.println("The price must be a positive number!");
                        return 0;
		}
	}


	

	public Object[][] showAll() {
		List<Products> products = new ArrayList<Products>();
		products = prodDAO.findAll();
                int nrLin = 0;
		for (Products product : products) {
			nrLin++;
		}
		Object[][] table = new Object[nrLin][Products.class.getDeclaredFields().length];
		int i = 0;
		for (Products product : products) {
			for (int j = 0; j < Products.class.getDeclaredFields().length; j++) {
				try {
					Field f = Products.class.getDeclaredFields()[j];
					f.setAccessible(true);
					table[i][j] = f.get(product);
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException ex) {
                                Logger.getLogger(ProductsBLL.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IllegalAccessException ex) {
                                Logger.getLogger(ProductsBLL.class.getName()).log(Level.SEVERE, null, ex);
                            }
			}
			i++;
		}
		return table;
	}
        
				
}
