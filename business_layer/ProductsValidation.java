
package business_layer;

import model.Products;
/**
 * – contine o singura variabila instanta, de tipul Products, 
 * care contine date referitoare la un produs; pe langa constructor, clasa contine si o metoda care se ocupa 
 * cu validarea informatiilor referitoare la un produs;
 */
public class ProductsValidation {
    
	private Products product;

	public ProductsValidation(Products product) {
		super();
		this.product = product;
	}

	public boolean isValid() {
		if (product.getProdID() > 0) {
			if (product.getProdName().length() <= 30) {
				if (product.getPrice() > 0) {
					if (product.getStock() > 0) {
						return true;
					} else {
						System.out.println("The stock must be a positive number!");
					}
				} else {
					System.out.println("the price must be a positive number!");
				}
			} else {
				System.out.println("The client's name is too long!");
			}
		} else {
			System.out.println("The client's ID must be a positive number!");
		}
		return false;
	}
}

