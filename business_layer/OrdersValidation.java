
package business_layer;

import model.Orders;
/**
 * – contine o singura variabila instanta, de tipul Orders, care cuprinde informatii referitoare la o comanda; 
 * - pe langa constructor, clasa contine si o metoda care se ocupa cu validarea datelor referitoare la o comanda;
 */
public class OrdersValidation {
    
	private Orders order;

	public OrdersValidation(Orders order) {
		super();
		this.order = order;
	}

	public boolean isValid() {
		if (order.getOrdID() > 0) {
			if (order.getClID() > 0) {
				if (order.getProdID() > 0) {
					if (order.getNrBuc() > 0) {
						return true;
					} else {
						System.out.println("The quantity of product must be a positive number!");
					}
				} else {
					System.out.println("The product's ID must be a positive number!");
				}
			} else {
				System.out.println("The client's ID must be a positive number!");
			}
		} else {
			System.out.println("The order's ID must be a positive number!");
		}
		return false;
	}
}
