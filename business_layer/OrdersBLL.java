
package business_layer;

import java.lang.reflect.Field;
import java.util.ArrayList;

import dao_package.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.*;
/**
 * - contine patru variabile instanta: trei obiecte de tipul OrdersAbstractDAO, ProductsAbstractDAO, 
 * respectiv ClientsAbstractDAO, care au rolul de a efectua legatura intre Business Logic Layer si Data Access Layer, 
 * si un obiect de tipul OrdersValidation, care are rolul de a valida datele referitoare la o comanda 
 * inainte de efectuarea unei operatii asupra acesteia; 
 * - clasa contine constructor si mai contine si cate o metoda pentru fiecare operatie de baza asupra tabelului Orders 
 * (adaugarea unei comenzi, stergerea unei comenzi, editarea produsului comenzii, editarea cantitatii comenzii,
 * editarea clientului comenzii si vizualizarea tuturor comenzilor din tabela Orders);
 */
public class OrdersBLL {
        
	private OrdersAbstractDAO ordDAO;
        private ProductsAbstractDAO prodDAO;
	private ClientsAbstractDAO clDAO;
	private OrdersValidation ordValidation;

	public OrdersBLL(OrdersAbstractDAO ordDAO, ProductsAbstractDAO prodDAO, ClientsAbstractDAO clDAO,
			OrdersValidation ordValidation) {
		super();
		this.ordDAO = ordDAO;
                this.prodDAO = prodDAO;
		this.clDAO = clDAO;
		this.ordValidation = ordValidation;
	}

	public int addOrder(Orders order) {
		if (ordValidation.isValid()) {
			if (ordDAO.findByID(order.getOrdID()) == null) {
				if (clDAO.findByID(order.getClID()) != null) {//verificare existenta client
					if (prodDAO.findByID(order.getProdID()) != null) {//verificare existenta produs in lista de produse comercializate de depozit
						if (prodDAO.findByID(order.getProdID()).getStock() >= order.getNrBuc()) {
							ordDAO.addObject(order);
                                                        int newStock=prodDAO.findByID(order.getProdID()).getStock() - order.getNrBuc();
							prodDAO.updateField("stock","prodID",
									newStock, order.getProdID());
                                                        return 1;
						} else {
							System.out.println("Under stock!");
                                                        return 0;
                                                        
						}
					} else {
						System.out.println("This product does not exist!");
                                                return 0;
                                                
					}
				} else {
					System.out.println("This client does not exist!");
                                        return 0;
                                        
				}
			} else {
				System.out.println("This order's ID already exists!");
                                return 0;
			}
		}
                return 0;
	}
        public void deleteOrder(int OrdID) {
		if (ordDAO.findByID(OrdID) != null) {
			if (prodDAO.findByID(ordDAO.findByID(OrdID).getProdID()) != null) {
				prodDAO.updateField("stock", "prodID", prodDAO.findByID(ordDAO.findByID(OrdID).getProdID()).getStock()
						+ ordDAO.findByID(OrdID).getNrBuc(), ordDAO.findByID(OrdID).getProdID());
				ordDAO.deleteObjectByID(ordDAO.findByID(OrdID).getOrdID());
			} else {
				System.out.println("This product does not exist!");
			}
		} else {
			System.out.println("This order does not exist!");
		}
	}
	public void editOrdProd(int OrdID, int ProdID) {
		if (ordDAO.findByID(OrdID) != null) {
			if (prodDAO.findByID(ProdID) != null) {
				if (prodDAO.findByID(ProdID).getStock() >= ordDAO.findByID(OrdID).getNrBuc()) {
					prodDAO.updateField("stock", "prodID",
							prodDAO.findByID(ordDAO.findByID(ordDAO.findByID(OrdID).getOrdID()).getProdID()).getStock()
									+ ordDAO.findByID(ordDAO.findByID(OrdID).getOrdID()).getNrBuc(),
							ordDAO.findByID(ordDAO.findByID(OrdID).getOrdID()).getProdID());//se pune din nou in stoc toata cantitatea de produs veche  
					ordDAO.updateField("prodID", "ordID", ProdID, ordDAO.findByID(OrdID).getOrdID());//se schimba produsul ce se doreste a fi comandat
					prodDAO.updateField("stock", "prodID",
							prodDAO.findByID(ProdID).getStock() - ordDAO.findByID(OrdID).getNrBuc(), ProdID);//se modifica stocul noului tip de produs dorit
                                        
				} else {
					System.out.println("Under stock!");
				}
			} else {
				System.out.println("The product does not exist!");
			}
		} else {
			System.out.println("The order does not exist!");
		}
	}

	public void editOrdNrBuc(int OrdID, int newNrBuc) {
		if (newNrBuc > 0) {
			if (prodDAO.findByID(ordDAO.findByID(OrdID).getProdID()) != null) {
				if (prodDAO.findByID(ordDAO.findByID(OrdID).getProdID()).getStock()+ ordDAO.findByID(OrdID).getNrBuc() >= newNrBuc) {
					prodDAO.updateField(
							"stock", "prodID", prodDAO.findByID(ordDAO.findByID(OrdID).getProdID()).getStock()
									 + ordDAO.findByID(OrdID).getNrBuc()- newNrBuc,
							ordDAO.findByID(OrdID).getProdID());
					ordDAO.updateField("nrBuc", "ordID", newNrBuc, OrdID);
				} else {
					System.out.println("Under stock!");
				}
			} else {
				System.out.println("The product does not exist in stock!");
			}
		} else {
			System.out.println("The new quantity of product must be a positive number!");
		}
	}
	public void editOrdCl(int OrdID, int ClID) {
		if (ClID > 0) {
			if (clDAO.findByID(ClID) != null) {
				ordDAO.updateField("clID", "ordID", ClID, OrdID);
			} else {
				System.out.println("This client does not exist!");
			}
		} else {
			System.out.println("Client's ID must be a positive number!");
		}
	}



	

	public Object[][] showAll() {
		List<Orders> orders = new ArrayList<Orders>();
		orders = ordDAO.findAll();
                int nrLin = 0;
		for (Orders order : orders) {
			nrLin++;
		}
		Object[][] table = new Object[nrLin][Orders.class.getDeclaredFields().length];
		int i = 0;
		for (Orders order : orders) {
			for (int j = 0; j < Orders.class.getDeclaredFields().length; j++) {
				try {
					Field f = Orders.class.getDeclaredFields()[j];
					f.setAccessible(true);
					table[i][j] = f.get(order);
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException ex) {
                                Logger.getLogger(ClientsBLL.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IllegalAccessException ex) {
                                Logger.getLogger(ClientsBLL.class.getName()).log(Level.SEVERE, null, ex);
                            }
			}
			i++;
		}
		return table;
	}
        public Products getProd(int id) {
		
		return prodDAO.findByID(id);
        }
        
}
