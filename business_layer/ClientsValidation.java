
package business_layer;

import model.Clients;
/**
 * – contine o singura variabila instanta, de tipul Clients, care cuprinde datele referitoare la un client; 
 * - pe langa constructor, clasa contine si o metoda care se ocupa cu validarea datelor ce caracterizeaza un client;
 */
public class ClientsValidation {
    
	private Clients client;

	public ClientsValidation(Clients client) {
		this.client = client;
	}

	public boolean isValid() {
		if (client.getID() > 0) {
			if (client.getName().length() <= 30) {
				return true;
			} else {
				System.out.println("The client's name is too long!");
			}
                }
		else {
			System.out.println("The client's ID must be a positive number!");
		}
		return false;
	}

}
