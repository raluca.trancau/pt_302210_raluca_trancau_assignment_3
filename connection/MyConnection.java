
package connection;
import java.sql.*;
import javax.swing.JOptionPane;



/**
 * – nu contine variabile instanta si nici constructor; in schimb, clasa contine metode pentru: 
 * obtinerea conexiunii la baza de date, inchiderea conexiunii, inchiderea ResultSet-ului si inchiderea PreparedStatement-ului;
 */
public class MyConnection{
        //private static java.sql.Connection conn = null;
    
public static Connection getConnection(Connection conn){
        
        try {
      Class.forName("com.mysql.jdbc.Driver");
      conn = DriverManager.getConnection("jdbc:mysql://localhost/warehouse","root","");
      return conn;
        }
        catch (Exception e){
             JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
             return null;
            
        }
       
    }
public static void closeConnection(Connection conn){
		
        if (conn != null) 		
        { 
           try 
           {
        	  
        	   conn.close();   
        	   conn = null;	
                   
           }
           catch(SQLException e) 
           {
        	   
        	   JOptionPane.showMessageDialog(null, "Cannot close conection to your database!...",
						  "Error", JOptionPane.ERROR_MESSAGE);
                   
           }    
        }
        
}
	

      
    
    public static void closeResultSet(ResultSet resultSet) {
		try {
			resultSet.close();
		} catch (SQLException ex) {
			System.err.println("Exception during resultSet close: " + ex);
		}
	}

	public static void closePreparedStatement(PreparedStatement statement) {
		try {
			statement.close();
		} catch (SQLException ex) {
			System.err.println("Exception during statement close: " + ex);
		}
	}
}
