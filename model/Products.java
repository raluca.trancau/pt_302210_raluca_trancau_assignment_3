
package model;
/**
 * - contine patru variabile instanta: id-ul produsului (de tip int), numele produsului (de tip String), pretul produsului (de tip float) si stocul (de tip int); 
 * - clasa contine doi constructori (unul cu parametri si celalalt fara), 
 * metode de get si set, pentru a accesa si modifica variabilele instanta din exteriorul acestei clase, 
 * si metoda toString, folosita pentru afisarea datelor referitoare la un produs;
 */
public class Products {
    
	private int prodID;
	private String prodName;
	private float price;
	private int stock;

	public Products(int prodID, String prodName, float price, int stock) {
		super();
		this.prodID = prodID;
		this.prodName = prodName;
		this.price = price;
		this.stock = stock;
	}
        public Products() {
		super();
		
	}
	public int getProdID() {
		return prodID;
	}
	public void setProdID(int prodID) {
		this.prodID = prodID;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	
	



	@Override
	public String toString() {
		return "Products [prodID=" + prodID + ", prodName=" + prodName + ", price=" + price + ", stock=" + stock + "]";
	}

}

