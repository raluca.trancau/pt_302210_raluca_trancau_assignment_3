
package model;
/**
 * – contine patru variabile instanta: id-ul comenzii, id-ul clientului, id-ul produsului si cantitatea de produs comandata (de tip int); 
 * - clasa contine doi constructori (unul cu parametri si celalalt fara), 
 *  metode de get si set, pentru a accesa si modifica variabilele instanta din exteriorul acestei clase, 
 *  si metoda toString, folosita pentru afisarea datelor referitoare la o comanda;
 */
public class Orders {
    
	private int ordID;
	private int clID;
	private int prodID;
	private int nrBuc;

	public Orders(int ordID, int clID, int prodID, int nrBuc) {
		super();
		this.ordID = ordID;
		this.clID = clID;
		this.prodID = prodID;
		this.nrBuc = nrBuc;
	}

	public Orders() {
		super();
	}

	public int getOrdID() {
		return ordID;
	}

	public int getClID() {
		return clID;
	}

	public int getProdID() {
		return prodID;
	}

	public int getNrBuc() {
		return nrBuc;
	}

	public void setOrdID(int ordID) {
		this.ordID = ordID;
	}

	public void setClID(int clID) {
		this.clID = clID;
	}

	public void setProdID(int prodID) {
		this.prodID = prodID;
	}

	public void setNrBuc(int nrBuc) {
		this.nrBuc = nrBuc;
	}

	@Override
	public String toString() {
		return "Orders [ordID=" + ordID + ", clID=" + clID + ", prodID=" + prodID + ", nrBuc=" + nrBuc + "]";
	}

}
