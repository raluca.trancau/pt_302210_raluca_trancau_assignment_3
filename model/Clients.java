
package model;
/**
 * – contine doua variabile instanta: id-ul clientului (de tip int) si numele clientului (de tip String);
 * - clasa mai contine doi constructori (unul cu parametri si celalalt fara), 
 * metode de get si set, pentru a accesa si modifica variabilele instanta din exteriorul acestei clase, 
 * si metoda toString, folosita pentru afisarea datelor referitoare la un client;
 */
public class Clients {
    
	private int ID;
	private String name;

	public Clients(int ID, String name) {
		super();
		this.ID = ID;
		this.name = name;
	}

	public Clients() {
		super();
	}

	public int getID() {
		return ID;
	}
        public void setID(int ID) {
		this.ID = ID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Clients [ID=" + ID + ", name=" + name + "]";
	}

}
