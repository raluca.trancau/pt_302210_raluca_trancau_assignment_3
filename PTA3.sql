-- Baza de date WAREHOUSE --



-- -------------------------------------------------

CREATE TABLE Products (
	prodID integer,
	prodName varchar (30),
	price float,
	stock integer);

CREATE TABLE Orders (
	ordID integer,
	clID integer,
	prodID integer,
        nrBuc integer);

CREATE TABLE Clients (
	ID integer,
	name varchar (30));


Insert into Products (prodID,prodName,price,stock) values ('1','apple','1',10);
Insert into Products (prodID,prodName,price,stock) values ('2','grapes','2',20);
Insert into Products (prodID,prodName,price,stock) values ('3','strawberry','5',25);

Insert into Orders (ordID,clID,prodID,nrBuc) values ('1','3','2','4');
Insert into Orders (ordID,clID,prodID,nrBuc) values ('2','1','3','5');
Insert into Orders (ordID,clID,prodID,nrBuc) values ('3','2','1','2');

Insert into Clients (ID,name) values ('1','Turcu Alexandru');
Insert into Clients (ID,name) values ('2','Safta Sorin');
Insert into Clients (ID,name) values ('3','Moldovan Ana');


ALTER TABLE Products ADD CONSTRAINT PK_Prod PRIMARY KEY (prodID);

ALTER TABLE Orders ADD CONSTRAINT PK_Ord PRIMARY KEY (ordID);

ALTER TABLE Clients ADD CONSTRAINT PK_Cl PRIMARY KEY (ID);


ALTER TABLE Orders ADD 
	CONSTRAINT FK_Ord_Cl FOREIGN KEY (clID) REFERENCES Clients (ID);
ALTER TABLE Orders ADD 
	CONSTRAINT FK_Ord_Prod FOREIGN KEY (prodID) REFERENCES Products (prodID);
