
package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import business_layer.ProductsBLL;
import business_layer.ProductsValidation;
import dao_package.ProductsAbstractDAO;
import model.Products;

public class ControllerProducts {
	private ViewProducts v;
	private ProductsBLL prodBLL;

	public ControllerProducts(ViewProducts v) {
		super();
		this.v = v;
		this.v.addAddBtnListener(new ActionListenerAdd());
		this.v.addEditBtnListener(new ActionListenerEdit());
		this.v.addDeleteBtnListener(new ActionListenerDelete());
		this.v.addShowAllBtnListener(new ActionListenerShowAll());
		this.v.addBackBtnListener(new ActionListenerBack());
	}

	public class ActionListenerAdd implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				String prodIDString = v.getProdID().getText();
				int prodID = Integer.parseInt(prodIDString);
				String prodName = v.getProdName().getText();
				String prodPriceString = v.getProdPrice().getText();
				float prodPrice = Float.parseFloat(prodPriceString);
				String prodStockString = v.getProdStock().getText();
				int prodStock = Integer.parseInt(prodStockString);
				Products prod = new Products(prodID, prodName, prodPrice, prodStock);
				prodBLL = new ProductsBLL(new ProductsAbstractDAO(), new ProductsValidation(prod));
				prodBLL.addProduct(prod);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
		}
	}

	public class ActionListenerEdit implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				String prodIDString = v.getProdIDEdit().getText();
				int prodID = Integer.parseInt(prodIDString);
				Products prod = new Products(0, "", 0, 0);
				prodBLL = new ProductsBLL(new ProductsAbstractDAO(), new ProductsValidation(prod));
				if (!v.getNewProdName().getText().equals("")) {
					String prodNewName = v.getNewProdName().getText();
					prodBLL.editProdName(prodID, prodNewName);
				}
				if (!v.getNewProdPrice().getText().equals("")) {
					String prodNewPriceString = v.getNewProdPrice().getText();
					float prodPrice = Float.parseFloat(prodNewPriceString);
					prodBLL.editProdPrice(prodID, prodPrice);
				}
				if (!v.getNewProdStock().getText().equals("")) {
					String prodNewStockString = v.getNewProdStock().getText();
					int prodStock = Integer.parseInt(prodNewStockString);
					prodBLL.editProdStock(prodID, prodStock);
				}
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
		}

	}

	public class ActionListenerDelete implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				String prodIDString = v.getProdIDDelete().getText();
				int prodID = Integer.parseInt(prodIDString);
				Products prod = new Products(0, "", 0, 0);
				prodBLL = new ProductsBLL(new ProductsAbstractDAO(), new ProductsValidation(prod));
				prodBLL.deleteProduct(prodID);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
		}

	}

	public class ActionListenerShowAll implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			v.getModel().setRowCount(0);
			Object[][] table = new Object[10000][];
			Products prod = new Products(0, "", 0, 0);
			prodBLL = new ProductsBLL(new ProductsAbstractDAO(), new ProductsValidation(prod));
			table = prodBLL.showAll();
			for (int i = 0; i < table.length; i++) {
				v.getModel().addRow(table[i]);
			}
		}

	}

	public class ActionListenerBack implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			v.setVisible(false);
			ViewStart view = new ViewStart();
			ControllerViewStart control = new ControllerViewStart(view);
		}

	}

}
