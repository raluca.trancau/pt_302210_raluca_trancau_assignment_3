
package presentation;
import business_layer.ClientsBLL;
import business_layer.ClientsValidation;
import business_layer.OrdersBLL;
import business_layer.OrdersValidation;
import business_layer.ProductsBLL;
import business_layer.ProductsValidation;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import dao_package.*;
import connection.MyConnection;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.Clients;
import model.Orders;
import model.Products;


/**
 * Contine o varianta alternativa de UI, prin intermediul careia comenzile se citesc dintr-un fisier text si se genereaza documente pdf pentru vizualizarea datelor dorite din BD.
 * In implementarea clasei, am folosit expresii regulate si pattern matching cu ajutorul carora am interpretat comenzile din fisierul txt, urmand ca, in functie de caz, 
 * sa se efectueze operatiile corespunzatoare asupra BD si sa se genereze rapoartele, facturile sau mesajul de eroare in cazul in care nu se poate procesa o comanda, toate in format pdf.
 */
public class GenPDFReports {
	

	
        public static void main(String[] args) {
        Scanner f = null;
        ClientsBLL clBLL=null;
        ProductsBLL prodBLL=null;
        ProductsAbstractDAO prodDAO=null;
        OrdersAbstractDAO ordDAO=null;
        ClientsAbstractDAO clDAO=null;
	OrdersBLL ordBLL = null;
        int index=0;//indexul unui raport pentru afisare tabel
        int nrCrt=0;//nr facturii curente sau al erorii corespunzatoare facturii curente care nu se poate procesa
        
		
		try {
			f = new Scanner(new File(args[0]));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		while(f.hasNextLine()){
			String line=f.nextLine();
                        if(line.startsWith("Insert client")==true)
                        {   
                            Pattern pat = Pattern.compile("([a-zA-Z]{1,})(\\s)([a-zA-Z]{1,})(\\:\\s)([0-9]{1,})(\\,\\s)([a-zA-Z]{1,}\\s[a-zA-Z]{1,})");
                            Matcher mat = pat.matcher(line);
	                    if(mat.find()) {
                               try {
				int clID = Integer.parseInt(mat.group(5));
				String clName = mat.group(7);
				Clients cl = new Clients(clID, clName);
				clBLL = new ClientsBLL(new ClientsAbstractDAO(), new ClientsValidation(cl));
				clBLL.addClient(cl);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
	                }
                         
                        }
                      else if(line.startsWith("Delete client")==true)
                        {   
                            Pattern pat = Pattern.compile("([a-zA-Z]{1,})(\\s)([a-zA-Z]{1,})(\\:\\s)([0-9]{1,})");
                            Matcher mat = pat.matcher(line);
	                    if(mat.find()) {
                               try {
				
				int clID = Integer.parseInt(mat.group(5));
				Clients cl = new Clients(0, "");
				clBLL = new ClientsBLL(new ClientsAbstractDAO(), new ClientsValidation(cl));
				clBLL.deleteClient(clID);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
	                }
                         
                        }
                         else if(line.startsWith("Insert product")==true)
                        {   int ok1=0,ok2=0,ok3=0;
                            Pattern pat = Pattern.compile("([a-zA-Z]{1,})(\\s)([a-zA-Z]{1,})(\\:\\s)([0-9]{1,})(\\,\\s)([a-zA-Z]{1,})(\\,\\s)(\\d*\\.?\\d*)(\\,\\s)([0-9]{1,})");
                            Matcher mat = pat.matcher(line);
	                    if(mat.find()) {
                                int prodID = Integer.parseInt(mat.group(5));
				String prodName = mat.group(7);
				float prodPrice = Float.parseFloat(mat.group(9));
				int prodStock = Integer.parseInt(mat.group(11));
				Products prod = new Products(prodID, prodName, prodPrice, prodStock);
				prodBLL = new ProductsBLL(new ProductsAbstractDAO(), new ProductsValidation(prod));
                               
                                try{
				prodBLL.addProduct(prod);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
	                }
                         
                        }
                        else if(line.startsWith("Delete product")==true)
                        {   
                            Pattern pat = Pattern.compile("([a-zA-Z]{1,})(\\s)([a-zA-Z]{1,})(\\:\\s)([0-9]{1,})");
                            Matcher mat = pat.matcher(line);
	                    if(mat.find()) {
                               try {
				
				int prodID = Integer.parseInt(mat.group(5));
				Products prod = new Products(0, "", 0, 0);
				prodBLL = new ProductsBLL(new ProductsAbstractDAO(), new ProductsValidation(prod));
				prodBLL.deleteProduct(prodID);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
                        
                        
		        }
                        }
                        else if(line.startsWith("Order")==true)
                        {   
                            int ok=0;
                            int ordID=0, clID=0, prodID=0, nrBuc=0;
                            Pattern pat = Pattern.compile("([a-zA-Z]{1,})(\\:\\s)([0-9]{1,})(\\,\\s)([0-9]{1,})(\\,\\s)([0-9]{1,})(\\,\\s)([0-9]{1,})");
                            Matcher mat = pat.matcher(line);
	                    if(mat.find()) {
                               try {
				
				ordID = Integer.parseInt(mat.group(3));
				clID = Integer.parseInt(mat.group(5));
				prodID = Integer.parseInt(mat.group(7));
				nrBuc = Integer.parseInt(mat.group(9));
				Orders ord = new Orders(ordID, clID, prodID, nrBuc);
                                prodDAO=new ProductsAbstractDAO();
                                ordDAO=new OrdersAbstractDAO();
                                clDAO=new ClientsAbstractDAO();
				ordBLL = new OrdersBLL(ordDAO, prodDAO, clDAO,
						new OrdersValidation(ord));
				ok=ordBLL.addOrder(ord);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
                        if(ok==1){nrCrt++;
                            Document d=new Document();
                                   try {
                                       PdfWriter.getInstance(d, new FileOutputStream("bill"+nrCrt+".pdf"));
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   } catch (FileNotFoundException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                        PdfPTable pt=new PdfPTable(4);
                        d.open();
                                   try {
                                       d.add(new Paragraph("Bill"));
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
			
				pt.addCell(("The ordID is "+ordID));
                                pt.addCell(("The clID is "+clID));
                                pt.addCell(("The product is "+ordBLL.getProd(prodID).getProdName()));
                                pt.addCell(("The price is "+ordBLL.getProd(prodID).getPrice()));
                                pt.addCell(("The nrBuc is "+nrBuc));
                                
			
                                   try {
                                       d.add(pt);
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                                   d.close();
                        
                        }
                        else {nrCrt++;
                            Document d=new Document();
                                   try {
                                       PdfWriter.getInstance(d, new FileOutputStream("error"+nrCrt+".pdf"));
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   } catch (FileNotFoundException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                        
                        d.open();
                                   try {
                                       d.add(new Paragraph("The order cannot be processed."));
                                       
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }

                                   d.close();
                        
                        }
                        }
                        
         
		        }
                        
                        
                else if(line.startsWith("Report")==true)
                        {   
                            Pattern pat = Pattern.compile("([a-zA-Z]{1,})(\\s)([a-zA-Z]{1,})");
                            Matcher mat = pat.matcher(line);
	                    if(mat.find()) {
                               if(mat.group(3).equals("client")==true)
                               {index++;
			Object[][] table = new Object[10000][];
			Clients cl = new Clients(0, "");
			clBLL = new ClientsBLL(new ClientsAbstractDAO(), new ClientsValidation(cl));
			table = clBLL.showAll();
                        Document d=new Document();
                                   try {
                                       PdfWriter.getInstance(d, new FileOutputStream("clients"+index+".pdf"));
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   } catch (FileNotFoundException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                        PdfPTable pt=new PdfPTable(2);
                        d.open();
                                   try {
                                       d.add(new Paragraph("Clients report"));
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                        pt.addCell(("ID"));
                        pt.addCell(("Name"));
			for (int i = 0; i < table.length; i++) {
                            
                                pt.addCell((""+table[i][0]));
                                pt.addCell((""+table[i][1]));
                            
			}
                                   try {
                                       d.add(pt);
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                                   d.close();
                        
                               }
                               else if(mat.group(3).equals("order")==true)
                               {index++;
			Object[][] table = new Object[10000][];
			Orders ord = new Orders(0, 0, 0, 0);
			ordBLL = new OrdersBLL(new OrdersAbstractDAO(),new ProductsAbstractDAO(), new ClientsAbstractDAO(),
					new OrdersValidation(ord));
			table = ordBLL.showAll();
                        Document d=new Document();
                                   try {
                                       PdfWriter.getInstance(d, new FileOutputStream("orders"+index+".pdf"));
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   } catch (FileNotFoundException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                        PdfPTable pt=new PdfPTable(4);
                        d.open();
                                   try {
                                       d.add(new Paragraph("Orders report"));
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                        pt.addCell(("OrdID"));
                        pt.addCell(("ClID")); 
                        pt.addCell(("ProdID"));
                        pt.addCell(("NrBuc"));
			for (int i = 0; i < table.length; i++) {
				pt.addCell((""+table[i][0]));
                                pt.addCell((""+table[i][1]));
                                pt.addCell((""+table[i][2]));
                                pt.addCell((""+table[i][3]));
			}
                                   try {
                                       d.add(pt);
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                                   d.close();
                        
                               }
                               else if(mat.group(3).equals("product")==true)
                               {index++;
			
			Object[][] table = new Object[10000][];
			Products prod = new Products(0, "", 0, 0);
			prodBLL = new ProductsBLL(new ProductsAbstractDAO(), new ProductsValidation(prod));
			table = prodBLL.showAll();
                        Document d=new Document();
                                   try {
                                       PdfWriter.getInstance(d, new FileOutputStream("products"+index+".pdf"));
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   } catch (FileNotFoundException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                        PdfPTable pt=new PdfPTable(4);
                        d.open();
                                   try {
                                       d.add(new Paragraph("Products report"));
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                        pt.addCell(("ID"));
                        pt.addCell(("Name"));
                        pt.addCell(("Price"));
                        pt.addCell(("Stock"));
			for (int i = 0; i < table.length; i++) {
				pt.addCell((""+table[i][0]));
                                pt.addCell((""+table[i][1]));
                                pt.addCell((""+table[i][2]));
                                pt.addCell((""+table[i][3]));
			}
                                   try {
                                       d.add(pt);
                                   } catch (DocumentException ex) {
                                       Logger.getLogger(GenPDFReports.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                                   d.close();
                        
                               }
                               else System.out.println("The command is not valid!");
                        
                        
                        
		        }
                           
                        } 
                        else System.out.println("The command is not valid!");
                }
		f.close();
        }
}

