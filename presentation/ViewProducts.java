
package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import model.Products;

public class ViewProducts extends JFrame {
	private JTextField prodID = new JTextField(20);
	private JTextField prodName = new JTextField(20);
	private JTextField prodPrice = new JTextField(20);
	private JTextField prodStock = new JTextField(20);
	private JTextField prodIDEdit = new JTextField(20);
	private JTextField newProdName = new JTextField(20);
	private JTextField newProdPrice = new JTextField(20);
	private JTextField newProdStock = new JTextField(20);
	private JTextField prodIDDelete = new JTextField(20);

	private JButton addBtn = new JButton("ADD");
	private JButton editBtn = new JButton("EDIT");
	private JButton delBtn = new JButton("DELETE");
	private JButton showAllBtn = new JButton("SHOW ALL");
	private JButton backBtn = new JButton("BACK");
	private JTable table = new JTable();
	private DefaultTableModel model = new DefaultTableModel();
	private JScrollPane jScrollPane = new JScrollPane();

	public ViewProducts() {
		this.setTitle("Products");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 560);

		JPanel firstPanel = new JPanel();
		firstPanel.setLayout(new BoxLayout(firstPanel, BoxLayout.X_AXIS));

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(Box.createRigidArea(new Dimension(0, 10)));
		panel.add(new JLabel("ADD A PRODUCT"));

		JPanel panelAdd = new JPanel();
		panelAdd.setLayout(new BoxLayout(panelAdd, BoxLayout.X_AXIS));
		panelAdd.add(Box.createRigidArea(new Dimension(7, 0)));

		JPanel panelAddLabel = new JPanel();
		panelAddLabel.setLayout(new BoxLayout(panelAddLabel, BoxLayout.Y_AXIS));
		panelAddLabel.add(Box.createRigidArea(new Dimension(0, 15)));
		panelAddLabel.add(new JLabel("Product ID"));
		panelAddLabel.add(Box.createRigidArea(new Dimension(0, 10)));
		panelAddLabel.add(new JLabel("Product Name"));
		panelAddLabel.add(Box.createRigidArea(new Dimension(0, 10)));
		panelAddLabel.add(new JLabel("Product Price"));
		panelAddLabel.add(Box.createRigidArea(new Dimension(0, 10)));
		panelAddLabel.add(new JLabel("Product Stock"));
		panelAddLabel.add(Box.createRigidArea(new Dimension(0, 20)));
		panelAdd.add(panelAddLabel);

		JPanel panelAddTF = new JPanel();
		panelAddTF.setLayout(new BoxLayout(panelAddTF, BoxLayout.Y_AXIS));
		panelAddTF.add(Box.createRigidArea(new Dimension(0, 20)));
		prodID.setMaximumSize(prodID.getPreferredSize());
		panelAddTF.add(prodID);
		panelAddTF.add(Box.createRigidArea(new Dimension(0, 5)));
		prodName.setMaximumSize(prodName.getPreferredSize());
		panelAddTF.add(prodName);
		panelAddTF.add(Box.createRigidArea(new Dimension(0, 5)));
		prodPrice.setMaximumSize(prodPrice.getPreferredSize());
		panelAddTF.add(prodPrice);
		panelAddTF.add(Box.createRigidArea(new Dimension(0, 5)));
		prodStock.setMaximumSize(prodStock.getPreferredSize());
		panelAddTF.add(prodStock);
		panelAddTF.add(Box.createRigidArea(new Dimension(0, 20)));

		panelAdd.add(panelAddTF);
		panelAdd.add(Box.createRigidArea(new Dimension(20, 0)));
		panelAdd.add(addBtn);
		panelAdd.add(Box.createRigidArea(new Dimension(11, 0)));
		panelAdd.setBorder(new LineBorder(Color.BLUE));
		panel.add(panelAdd);

		panel.add(Box.createRigidArea(new Dimension(0, 10)));
		panel.add(new JLabel("EDIT A PRODUCT"));

		JPanel panelEdit = new JPanel();
		panelEdit.setLayout(new BoxLayout(panelEdit, BoxLayout.X_AXIS));
		panelEdit.add(Box.createRigidArea(new Dimension(5, 0)));

		JPanel panelEditLabel = new JPanel();
		panelEditLabel.setLayout(new BoxLayout(panelEditLabel, BoxLayout.Y_AXIS));
		panelEditLabel.add(Box.createRigidArea(new Dimension(0, 15)));
		panelEditLabel.add(new JLabel("Product ID"));
		panelEditLabel.add(Box.createRigidArea(new Dimension(0, 10)));
		panelEditLabel.add(new JLabel("New Product Name"));
		panelEditLabel.add(Box.createRigidArea(new Dimension(0, 10)));
		panelEditLabel.add(new JLabel("New Product Price"));
		panelEditLabel.add(Box.createRigidArea(new Dimension(0, 10)));
		panelEditLabel.add(new JLabel("New Product Stock"));
		panelEditLabel.add(Box.createRigidArea(new Dimension(0, 20)));
		panelEdit.add(panelEditLabel);

		JPanel panelEditTF = new JPanel();
		panelEditTF.setLayout(new BoxLayout(panelEditTF, BoxLayout.Y_AXIS));
		panelEditTF.add(Box.createRigidArea(new Dimension(0, 20)));
		prodIDEdit.setMaximumSize(prodIDEdit.getPreferredSize());
		panelEditTF.add(prodIDEdit);
		panelEditTF.add(Box.createRigidArea(new Dimension(0, 5)));
		newProdName.setMaximumSize(newProdName.getPreferredSize());
		panelEditTF.add(newProdName);
		panelEditTF.add(Box.createRigidArea(new Dimension(0, 5)));
		newProdPrice.setMaximumSize(newProdPrice.getPreferredSize());
		panelEditTF.add(newProdPrice);
		panelEditTF.add(Box.createRigidArea(new Dimension(0, 5)));
		newProdStock.setMaximumSize(newProdStock.getPreferredSize());
		panelEditTF.add(newProdStock);
		panelEditTF.add(Box.createRigidArea(new Dimension(0, 20)));

		panelEdit.add(panelEditTF);
		panelEdit.add(Box.createRigidArea(new Dimension(0, 0)));
		panelEdit.add(editBtn);
		panelEdit.add(Box.createRigidArea(new Dimension(5, 0)));
		panelEdit.setBorder(new LineBorder(Color.BLUE));
		panel.add(panelEdit);

		panel.add(Box.createRigidArea(new Dimension(0, 10)));
		panel.add(new JLabel("DELETE A PRODUCT"));

		JPanel panelDelete = new JPanel();
		panelDelete.setLayout(new BoxLayout(panelDelete, BoxLayout.X_AXIS));
		panelDelete.add(Box.createRigidArea(new Dimension(7, 0)));

		JPanel panelDeleteLabel = new JPanel();
		panelDeleteLabel.setLayout(new BoxLayout(panelDeleteLabel, BoxLayout.Y_AXIS));
		panelDeleteLabel.add(Box.createRigidArea(new Dimension(0, 15)));
		panelDeleteLabel.add(new JLabel("Product ID"));
		panelDeleteLabel.add(Box.createRigidArea(new Dimension(0, 20)));
		panelDelete.add(panelDeleteLabel);

		JPanel panelDeleteTF = new JPanel();
		panelDeleteTF.setLayout(new BoxLayout(panelDeleteTF, BoxLayout.Y_AXIS));
		panelDeleteTF.add(Box.createRigidArea(new Dimension(0, 20)));
		prodIDDelete.setMaximumSize(prodIDDelete.getPreferredSize());
		panelDeleteTF.add(prodIDDelete);
		panelDeleteTF.add(Box.createRigidArea(new Dimension(0, 20)));

		panelDelete.add(panelDeleteTF);
		panelDelete.add(Box.createRigidArea(new Dimension(20, 0)));
		panelDelete.add(delBtn);
		panelDelete.add(Box.createRigidArea(new Dimension(13, 0)));
		panelDelete.setBorder(new LineBorder(Color.BLUE));
		panel.add(panelDelete);
		panel.add(Box.createRigidArea(new Dimension(0, 20)));

		JPanel panelViewAll = new JPanel();
		panelViewAll.setLayout(new BoxLayout(panelViewAll, BoxLayout.X_AXIS));
		panelViewAll.add(Box.createRigidArea(new Dimension(73, 0)));

		JPanel panelViewAllLabel = new JPanel();
		panelViewAllLabel.setLayout(new BoxLayout(panelViewAllLabel, BoxLayout.Y_AXIS));
		panelViewAllLabel.add(Box.createRigidArea(new Dimension(0, 10)));
		panelViewAllLabel.add(new JLabel("SHOW ALL PRODUCTS"));
		panelViewAllLabel.add(Box.createRigidArea(new Dimension(0, 10)));
		panelViewAll.add(panelViewAllLabel);

		panelViewAll.add(Box.createRigidArea(new Dimension(20, 0)));
		panelViewAll.add(showAllBtn);
		panelViewAll.add(Box.createRigidArea(new Dimension(73, 0)));
		panel.add(panelViewAll);

		panel.add(Box.createRigidArea(new Dimension(0, 10)));
		panel.add(backBtn);
		firstPanel.add(panel);

		JPanel panel1 = new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		panel1.add(Box.createRigidArea(new Dimension(0, 10)));
		String[] header = new String[Products.class.getDeclaredFields().length];
		int i = 0;
		for (Field field : Products.class.getDeclaredFields()) {
			header[i] = field.getName();
			i++;
		}
		model.setColumnIdentifiers(header);
		table.setModel(model);
		jScrollPane.setViewportView(table);
		panel1.add(jScrollPane);
		firstPanel.add(panel1);

		this.setContentPane(firstPanel);
		this.setLocation(200, 100);
		this.setVisible(true);
	}
        
	public JTable getTable() {
		return table;
	}

	public DefaultTableModel getModel() {
		return model;
	}

	public JScrollPane getjScrollPane() {
		return jScrollPane;
	}

	public void addAddBtnListener(ActionListener al) {
		addBtn.addActionListener(al);
	}

	public void addEditBtnListener(ActionListener al) {
		editBtn.addActionListener(al);
	}

	public void addDeleteBtnListener(ActionListener al) {
		delBtn.addActionListener(al);
	}

	public void addShowAllBtnListener(ActionListener al) {
		showAllBtn.addActionListener(al);
	}

	public void addBackBtnListener(ActionListener al) {
		backBtn.addActionListener(al);
	}


	public JTextField getProdID() {
		return prodID;
	}

	public JTextField getProdName() {
		return prodName;
	}

	public JTextField getProdPrice() {
		return prodPrice;
	}

	public JTextField getProdStock() {
		return prodStock;
	}

	public JTextField getProdIDEdit() {
		return prodIDEdit;
	}

	public JTextField getNewProdName() {
		return newProdName;
	}

	public JTextField getNewProdPrice() {
		return newProdPrice;
	}

	public JTextField getNewProdStock() {
		return newProdStock;
	}

	public JTextField getProdIDDelete() {
		return prodIDDelete;
	}
}
