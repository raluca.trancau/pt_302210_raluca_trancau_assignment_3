
package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControllerViewStart {
	private ViewStart v;

	public ControllerViewStart(ViewStart v) {
		super();
		this.v = v;
		this.v.addListenerComboBox(new ActionListenerComboBox());
	}

	public class ActionListenerComboBox implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String nextView = v.getComboBox().getSelectedItem().toString();
			
			if (nextView.equals("Clients")) {
				v.setVisible(false);
				ViewClients viewClients = new ViewClients();
				ControllerClients controlClients = new ControllerClients(viewClients);
			}
			if (nextView.equals("Orders")) {
				v.setVisible(false);
				ViewOrders viewOrders = new ViewOrders();
				ControllerOrders controlOrders = new ControllerOrders(viewOrders);
			}
                        if (nextView.equals("Products")) {
				v.setVisible(false);
				ViewProducts viewProducts = new ViewProducts();
				ControllerProducts controlProducts = new ControllerProducts(viewProducts);
			}
		}
	}
}
