
package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import business_layer.OrdersBLL;
import business_layer.OrdersValidation;
import dao_package.ClientsAbstractDAO;
import dao_package.OrdersAbstractDAO;
import dao_package.ProductsAbstractDAO;
import model.Orders;

public class ControllerOrders {
	private ViewOrders v;
	private OrdersBLL ordBLL;

	public ControllerOrders(ViewOrders v) {
		super();
		this.v = v;
		this.v.addAddBtnListener(new ActionListenerAdd());
		this.v.addEditBtnListener(new ActionListenerEdit());
		this.v.addDeleteBtnListener(new ActionListenerDelete());
		this.v.addShowAllBtnListener(new ActionListenerShowAll());
		this.v.addBackBtnListener(new ActionListenerBack());
	}

	public class ActionListenerAdd implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				String ordIDString = v.getOrdID().getText();
				int ordID = Integer.parseInt(ordIDString);
				String clIDString= v.getClID().getText();
				int clID = Integer.parseInt(clIDString);
				String prodIDString = v.getProdID().getText();
				int prodID = Integer.parseInt(prodIDString);
				String nrBucString = v.getNrBuc().getText();
				int nrBuc = Integer.parseInt(nrBucString);
				Orders ord = new Orders(ordID, clID, prodID, nrBuc);
				ordBLL = new OrdersBLL(new OrdersAbstractDAO(), new ProductsAbstractDAO(),new ClientsAbstractDAO(),
						new OrdersValidation(ord));
				ordBLL.addOrder(ord);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
		}
	}

	public class ActionListenerEdit implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				String ordIDString = v.getOrdIDEdit().getText();
				int ordID = Integer.parseInt(ordIDString);
				Orders ord = new Orders(0, 0, 0, 0);
				ordBLL = new OrdersBLL(new OrdersAbstractDAO(), new ProductsAbstractDAO(), new ClientsAbstractDAO(),
						new OrdersValidation(ord));
				if (!v.getNewClID().getText().equals("")) {
					String newClIDString = v.getNewClID().getText();
					int newClID = Integer.parseInt(newClIDString);
					ordBLL.editOrdCl(ordID, newClID);
				}
				if (!v.getNewProdID().getText().equals("")) {
					String newProdIDString = v.getNewProdID().getText();
					int newProdID = Integer.parseInt(newProdIDString);
					ordBLL.editOrdProd(ordID, newProdID);
				}
				if (!v.getNewNrBuc().getText().equals("")) {
					String newNrBucString = v.getNewNrBuc().getText();
					int newNrBuc = Integer.parseInt(newNrBucString);
					ordBLL.editOrdNrBuc(ordID, newNrBuc);
				}
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
		}

	}

	public class ActionListenerDelete implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				String ordIDString = v.getOrdIDDelete().getText();
				int ordID = Integer.parseInt(ordIDString);
				Orders ord = new Orders(0, 0, 0, 0);
				ordBLL = new OrdersBLL(new OrdersAbstractDAO(), new ProductsAbstractDAO(),new ClientsAbstractDAO(),
						new OrdersValidation(ord));
				ordBLL.deleteOrder(ordID);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
		}

	}

	public class ActionListenerShowAll implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			v.getModel().setRowCount(0);
			Object[][] table = new Object[10000][];
			Orders ord = new Orders(0, 0, 0, 0);
			ordBLL = new OrdersBLL(new OrdersAbstractDAO(),new ProductsAbstractDAO(), new ClientsAbstractDAO(),
					new OrdersValidation(ord));
			table = ordBLL.showAll();
			for (int i = 0; i < table.length; i++) {
				v.getModel().addRow(table[i]);
			}
		}

	}

	public class ActionListenerBack implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			v.setVisible(false);
			ViewStart view = new ViewStart();
			ControllerViewStart control = new ControllerViewStart(view);
		}

	}

}
