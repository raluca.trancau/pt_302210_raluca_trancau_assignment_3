
package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Clients;
import business_layer.ClientsBLL;
import business_layer.ClientsValidation;
import dao_package.ClientsAbstractDAO;


public class ControllerClients {
	
	private ClientsBLL clBLL;
        private ViewClients v;

	public ControllerClients(ViewClients v) {
		super();
		this.v= v;
		this.v.addAddBtnListener(new ActionListenerAdd());
		this.v.addEditBtnListener(new ActionListenerEdit());
		this.v.addDeleteBtnListener(new ActionListenerDelete());
		this.v.addShowAllBtnListener(new ActionListenerShowAll());
		this.v.addBackBtnListener(new ActionListenerBack());
	}

	public class ActionListenerAdd implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				String clIDString = v.getClID().getText();
				int clID = Integer.parseInt(clIDString);
				String clName = v.getClName().getText();
				Clients cl = new Clients(clID, clName);
				clBLL = new ClientsBLL(new ClientsAbstractDAO(), new ClientsValidation(cl));
				clBLL.addClient(cl);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
		}
	}

	public class ActionListenerEdit implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				String clIDString = v.getClIDEdit().getText();
				int clID = Integer.parseInt(clIDString);
				Clients cl = new Clients(0, "");
				clBLL = new ClientsBLL(new ClientsAbstractDAO(), new ClientsValidation(cl));
				if (!v.getNewClName().getText().equals("")) {
					String newClName = v.getNewClName().getText();
					clBLL.editClName(clID, newClName);
				}
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
		}

	}

	public class ActionListenerDelete implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				String clIDString = v.getClIDDelete().getText();
				int clID = Integer.parseInt(clIDString);
				Clients cl = new Clients(0, "");
				clBLL = new ClientsBLL(new ClientsAbstractDAO(), new ClientsValidation(cl));
				clBLL.deleteClient(clID);
			} catch (NumberFormatException ex) {
				System.out.println("The input is not valid!");
			}
		}

	}

	public class ActionListenerShowAll implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			v.getModel().setRowCount(0);
			Object[][] table = new Object[10000][];
			Clients cl = new Clients(0, "");
			clBLL = new ClientsBLL(new ClientsAbstractDAO(), new ClientsValidation(cl));
			table = clBLL.showAll();
			for (int i = 0; i < table.length; i++) {
				v.getModel().addRow(table[i]);
			}
		}

	}

	public class ActionListenerBack implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			v.setVisible(false);
			ViewStart view = new ViewStart();
			ControllerViewStart control = new ControllerViewStart(view);
		}

	}
}
